﻿using System;
using System.Collections.Generic;


namespace D1_majasdarbs
{
    class MainClass
    {
        public static void Main(string[] args)
        {


            List<SundayDataTypes> SundayDataTypesList = new List<SundayDataTypes>();

            Console.WriteLine("Izpildīt programmu? (y/n)");
            while(Console.ReadLine() != "n")

            {
                SundayDataTypes sundayData = new SundayDataTypes();

                Console.WriteLine("Svētdiena ir īsta? (t/f)");
                string BoolString = Console.ReadLine();
                sundayData.SundayBool = Convert.ToBoolean(BoolString);

                Console.WriteLine("Svētdienas baits?");
                string SundayByte = Console.ReadLine();
                bool isConverted = byte.TryParse(SundayByte, out byte result);
                if (isConverted == true)
                {
                    sundayData.SundayByte = Convert.ToByte(SundayByte);
                }

                Console.WriteLine("Svētdienas cipars?");
                string SundayNum = Console.ReadLine();
                sundayData.SundayNum = Convert.ToInt32(SundayNum);

                Console.WriteLine("Un kāds šodien burts?");
                string SundayChar = Console.ReadLine();
                sundayData.SundayChar = Convert.ToChar(SundayChar);

                Console.WriteLine("Svētdienas vārds?");
                sundayData.SundayString = Console.ReadLine();


                SundayDataTypesList.Add(sundayData);
            }

            // for(int i=0; i<SundayDataTypesList.Count; i++)
            foreach(SundayDataTypes sundayData in SundayDataTypesList)
            {
                Console.WriteLine("Mans cipars ir " + sundayData.SundayNum);
                Console.WriteLine("Mans baits ir " + sundayData.SundayByte);
                Console.WriteLine("Mans vārds " + sundayData.SundayString);
                Console.WriteLine("Mans burts ir " + sundayData.SundayChar);
                Console.WriteLine("Svētdiena ir " + sundayData.SundayBool);
            }







            /*
                    {
        List<string> userAnswers = new List<string>();
        foreach (QuizQuestion question in DefinedQuestions.quizQuestion)
        {
            Console.WriteLine(question.Question);
            string answer = Console.ReadLine();
            if (answer == question.Answer)
            {
                score++;
            }
            userAnswers.Add(answer);
        }

        for (int i = 0; i < userAnswers.Count; i++)
        {
            Console.WriteLine("Pareizā atbilde: " + DefinedQuestions.quizQuestion[i].Answer);
            Console.WriteLine("Tu atbildēji: " + userAnswers[i]);
        }
        Console.WriteLine("Punktu skaits: " + score + "\n"
                          + "Maksimālais punktu skaits bija: "+userAnswers.Count);
    }

 */


        }
    }
}
