﻿using System;
namespace D1_majasdarbs
{
    public class SundayDataTypes
    {
        public SundayDataTypes(){}

        public int SundayNum { get; set; }
        public char SundayChar { get; set; }
        public string SundayString { get; set; }
        public bool SundayBool { get; set; }
        public byte SundayByte { get; set; }
        public decimal SundayDecimal { get;  set;}
    }
}
