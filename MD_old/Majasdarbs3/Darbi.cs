﻿using System;
namespace Majasdarbs3
{
    public class Darbi
    {
        public Darbi()
        { }

        public string MiseneS {get; set;}
        public string MiseneM { get; set; }
        public string MiseneL { get; set; }
        public string KomoM { get; set; }
        public string KomoD { get; set; }
        public string KomoN { get; set; }
        public string KomoUdens { get; set; }
        public string Iela { get; set; }
        public string Brugis { get; set; }
        public string Aboli { get; set; }
        public string Lapas { get; set; }
        public string Zale { get; set; }
        public string Sniegs { get; set; }

    } 
    }
