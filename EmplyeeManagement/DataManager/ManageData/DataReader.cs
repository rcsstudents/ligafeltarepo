﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
namespace DataManager
{
    public class DataReader
    {
        // public T metode<T>() - generic metodes pieraksts, ar kuru var atgriezt jebkāda tipa vērtības, norādot kādas, piem., - int vertibaInt = metode<int>();


        public readonly string FileName;
        public DataReader (string fileName)
        {
            FileName = fileName;
        }

     /*   public Employee ReadEmployee()
        {
            using (StreamReader reader = new StreamReader(FileName))
            {
                string employeeData = reader.ReadToEnd();
                Employee employee =
                          JsonConvert.DeserializeObject<Employee>(employeeData);
                return employee;
            }
        }
        */

        public List<T> ReadData<T>()
        {
            using (StreamReader reader = new StreamReader(FileName))
            {
                string dataFromFile = reader.ReadToEnd();
                List<T> data = JsonConvert.DeserializeObject<List<T>>(dataFromFile);
                return data ?? new List<T>();
            }
        }

    }
}
