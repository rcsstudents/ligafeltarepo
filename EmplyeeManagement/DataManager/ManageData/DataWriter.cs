﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
namespace DataManager

{
    public class DataWriter
    {
        public readonly string FileName;

        public DataWriter(string fileName)
        {
            FileName = fileName;
        }


     /*   public void WriteEmployee(Employee employee)
        {
             using (StreamWriter writer = new StreamWriter(FileName))
             {
                 string employeeJson = JsonConvert.SerializeObject(employee);
                 writer.WriteLine(employeeJson);
             }
        }
        */

        public void WriteData<T>(List<T>data)
        {
            using (StreamWriter writer = new StreamWriter(FileName))
            {
                string dataJson = JsonConvert.SerializeObject(data);
                writer.WriteLine(dataJson);
            }
        }
    }
}
