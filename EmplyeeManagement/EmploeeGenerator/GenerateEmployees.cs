﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Models;

namespace EmployeeGenerator
{
    public class GenerateEmployees
    {


    private List<string> GenerateData(string dataType)
        {
            Console.WriteLine("Write " + dataType);
            string data = Console.ReadLine();
            List<string> dataList = data.Split(' ').ToList();

            return dataList;

        }

        public Vacation GenerateVacation()
        {
            List<string> vacationData = GenerateData("Vacation");
            Vacation vacation = new Vacation();
            vacation.Reason = vacationData[0];
            vacation.StartDate = new DateTime(
                Convert.ToInt32(vacationData[1]),
                Convert.ToInt32(vacationData[2]),
                Convert.ToInt32(vacationData[3]));
            vacation.EndDate = new DateTime(
                Convert.ToInt32(vacationData[4]),
                Convert.ToInt32(vacationData[5]),
                Convert.ToInt32(vacationData[6]));

            return vacation;
        }

        public Employee GenerateEmployee()
        {
            List<string> employeeData = GenerateData("Employee");
            Employee employee = new Employee();
            employee.Name = employeeData[0];
            employee.Surname = employeeData[1];
            employee.Age = Convert.ToInt32(employeeData[2]);
            employee.Vacation = GenerateVacation();

            return employee;
        }
    }
}
