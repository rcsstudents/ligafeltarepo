﻿using System;
using EmployeeGenerator;
using Models;
using System.IO;
using DataManager;


namespace EmployeeGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to Employee Generator!");
            try
            {
                GenerateEmployees generateEmployee = new GenerateEmployees();
                Employee employee = generateEmployee.GenerateEmployee();

        DataWriter writer = new DataWriter(DataConfiguration.EmployeeData);
                List<Employee> employees = new List<Employee>();
                employees.Add(employee);
                writer.WriteData(employees);

            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadKey();
            }
        }
    }
}
