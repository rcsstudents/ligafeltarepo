﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Diena3_uzdevumi
{
    public class Professor : PersonAbstract, IPerson
    {
        public Professor(string name, string surname)
        {
            Name = name;
            Surname = surname;
        }


        public string GetHomework()
        {
            fullName = GetFullName();
            return ("Professor " + fullName + " has no homework");
        }

    }
}
