﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Diena3_uzdevumi
{
    class MainClass
    {
        private static void Main(string[] args)
        {
            List<IPerson> personsList = new List<IPerson>();
            List<IPerson> personsList2 = new List<IPerson>();

            personsList.Add(new Student2("Anna", "Kanna"));
            personsList.Add(new Professor("Vilis", "Lācis"));

            foreach(IPerson person in personsList)
            {
                Console.WriteLine(person.GetHomework());
            }
            Console.WriteLine();

            Student2 student = new Student2("Tālis", "Vālis");
            Professor professor = new Professor("Juris", "Turis");

            Console.WriteLine(professor.More());
            Console.WriteLine(student.More());

            Console.WriteLine();

            string turpinat = "";
            while (turpinat != "n")
            {

                Console.WriteLine("Tu esi students (0) vai pasniedzējs (1)?");

            string amats = Console.ReadLine();
            int amatsInt = Convert.ToInt32(amats);
            PEnum amatsE = (PEnum)amatsInt;

            Console.WriteLine("Ievadi vārdu: ");
            string vards = Console.ReadLine();

            Console.WriteLine("Ievadi uzvārdu: ");
            string uzvards = Console.ReadLine();

                switch (amatsE)
                {

                case PEnum.StudentEnum:

                Student2 studentJauns = new Student2(vards, uzvards);
                personsList2.Add(studentJauns);
                        break;

                case PEnum.ProfessorEnum:
            
                Professor professorJauns = new Professor(vards, uzvards);
                    personsList2.Add(professorJauns);
                        break;
               

            }
                Console.WriteLine("Turpināt? y/n ");
                turpinat = Console.ReadLine();
            }

            foreach (IPerson person in personsList2)
            {
                Console.WriteLine(person.GetHomework());
            }



        }
    }
}
