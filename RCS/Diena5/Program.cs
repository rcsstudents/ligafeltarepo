﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;


namespace Diena5
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            // ierakstīt vienu rindu (pārraksta iepriekšējo) using (StreamWriter writer = new StreamWriter("teksts.txt"))
            {
            //    writer.WriteLine("Pirmais teksts failā!");

            }
            // papildināt tekstu, nenodzēšot iepriekšējo

            using(StreamWriter writer = File.AppendText("teksts.txt"))
            {
                writer.WriteLine("Pirmais teksts failā, update222");
            }

            //File.WriteAllText("teksts.txt", String.Empty); - izdzēst tekstu
            // ielasīt tekstu: 

            List<string> tekstaSaraksts = new List<string>();
            using (StreamReader reader = new StreamReader("teksts.txt"))
            {
                string line;
                while((line= reader.ReadLine()) != null)
                {
                    tekstaSaraksts.Add(line);
                }
            }
            // attēlot ielasīto tekstu:
            foreach(string tekstaRinda in tekstaSaraksts)
            {
                Console.WriteLine(tekstaRinda);
            }


            QuizQuestion quizQuestion = new QuizQuestion();
            quizQuestion.Question = "Jautājums";
            quizQuestion.Answer = "Atbilde";

            string json = JsonConvert.SerializeObject(quizQuestion);
            using (StreamWriter writer = new StreamWriter ("MyJson.txt"))
            {
                writer.WriteLine(json);
            }

            string jsonFromFile;
            using (StreamReader reader = new StreamReader("MyJson.txt"))
           
                {
                jsonFromFile = reader.ReadToEnd();
                }


            QuizQuestion deserialized = JsonConvert.DeserializeObject < QuizQuestion > (json);

            Console.WriteLine(deserialized.Question);
            Console.WriteLine(deserialized.Answer);
        }

    }

}
