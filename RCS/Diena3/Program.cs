﻿using System;
using System.Collections.Generic;

namespace Diena3
{
  enum DayOfWeekEnum
    {
        Monday = 0,
        Tuesday = 1,
        Wednesday = 2,
        Thursday = 3,
        Friday = 4,
        Saturday = 5,
        Sunday = 6

    }

    class MainClass
    {
        public static void Main(string[] args)
        {
           /* Console.WriteLine("Ievadi nedēļas dienu: ");
            string nedelasDiena = Console.ReadLine();
            int nedelasDienasNumurs = Convert.ToInt32(nedelasDiena);


            switch (nedelasDienasNumurs)
            {
                case (int)DayOfWeekEnum.Monday:
                    Console.WriteLine("Šī ir pirmdiena!");
                    break;
                case (int)DayOfWeekEnum.Tuesday:
                    Console.WriteLine("Šī ir otrdiena!");
                    break;
                case (int)DayOfWeekEnum.Wednesday:
                    Console.WriteLine("Šī ir trešdiena!");
                    break;
                case (int)DayOfWeekEnum.Thursday:
                    Console.WriteLine("Šī ir ceturtdiena!");
                    break;
                case (int)DayOfWeekEnum.Friday:
                    Console.WriteLine("Šī ir piektdiena!");
                    break;
                case (int)DayOfWeekEnum.Saturday:
                    Console.WriteLine("Šī ir sestdiena!");
                    break;
                case (int)DayOfWeekEnum.Sunday:
                    Console.WriteLine("Šī ir svētdiena!");
                    break;
                default:
                    Console.WriteLine("Ieraksti ciparu!");
                    break;
            } 
            */

            Console.WriteLine("Ievadi dienu: ");
            string diena = Console.ReadLine();
            int dienaNumurs = Convert.ToInt32(diena);

            Console.WriteLine("Ievadi mēnesi: ");
            string menesis = Console.ReadLine();
            int menesisNumurs = Convert.ToInt32(menesis);

            Console.WriteLine("Ievadi gadu: ");
            string gads = Console.ReadLine();
            int gadsNumurs = Convert.ToInt32(gads);

            DateTime datums = new DateTime(gadsNumurs, menesisNumurs, dienaNumurs);
            int dayOfWeek = (int)datums.DayOfWeek;
            // DayOfWeek dayOfWeek = datums.DayOfWeek;

            switch(dayOfWeek)
            {
                case (int)DayOfWeek.Monday:
                    Console.WriteLine("Šī ir pirmdiena!");
                    break;
                case (int)DayOfWeek.Tuesday:
                    Console.WriteLine("Šī ir otrdiena!");
                    break;
                case (int)DayOfWeek.Wednesday:
                    Console.WriteLine("Šī ir trešdiena!");
                    break;
                case (int)DayOfWeek.Thursday:
                    Console.WriteLine("Šī ir ceturtdiena!");
                    break;
                case (int)DayOfWeek.Friday:
                    Console.WriteLine("Šī ir piektdiena!");
                    break;
                case (int)DayOfWeek.Saturday:
                    Console.WriteLine("Šī ir sestdiena!");
                    break;
                case (int)DayOfWeek.Sunday:
                    Console.WriteLine("Šī ir svētdiena!");
                    break;
                default:
                    Console.WriteLine("Neatrodu!");
                    break;

            }
            if(dayOfWeek==(int)DayOfWeek.Sunday){ // if(dayOfWeek==DayOfWeek.Saturday || dayOfWeek.Sundy)
                Console.WriteLine("Brīvdiena");
            }
            else if (dayOfWeek == (int)DayOfWeek.Saturday)
            {
                Console.WriteLine("Brīvdiena");
            }
            else
            {
                Console.WriteLine("Darba diena");
            }

            string laiks = DateTime.Now.ToString("h:mm:ss tt");
            DateTime datumsSodien = DateTime.Now;

            Console.WriteLine();
            Console.WriteLine("Pareizs laiks šobrīd: " + laiks);

            Console.ReadKey();

        }

    }
}
