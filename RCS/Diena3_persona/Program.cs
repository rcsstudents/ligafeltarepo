﻿using System;
using System.Collections.Generic;

namespace Diena3_persona
{
    class MainClass
    {
        static void Main(string[] args)
        {
            // Diena3();

            Animal animal = new Animal();
            Person person = new Person("Līga", 173, 70, "brūna", DayOfWeek.Saturday);

            animal.Eat();
            animal.Drink();

            person.Eat();
            person.Drink();
        }

        public static void Diena3()
        {
            List<IWalking> walkingList = new List<IWalking>();
            List<Person> personList = new List<Person>();
            List<Programmer> programmersList = new List<Programmer>();

            string turpinat = "";
            while (turpinat != "n")
            {
                Console.WriteLine("Ievadi vārdu: ");
                string vards = Console.ReadLine();

                Console.WriteLine("Ievadi augumu: ");
                string augums = Console.ReadLine();
                double augumsDouble = Convert.ToDouble(augums);

                Console.WriteLine("Ievadi svaru: ");
                string svars = Console.ReadLine();
                double svarsDouble = Convert.ToDouble(svars);

                Console.WriteLine("Ievadi matu krāsu: ");
                string matuKrasa = Console.ReadLine();

                Console.WriteLine("Mīļākā nedēļas diena? (1-7) ");
                string dayOfWeek = Console.ReadLine();
                int dayOfWeekInt = Convert.ToInt32(dayOfWeek);
                DayOfWeek nedelasDiena = (DayOfWeek)dayOfWeekInt;

                Console.WriteLine("Programmēšanas valoda?");
                string valoda = Console.ReadLine();

                Person cilveks = new Person(vards, augumsDouble, svarsDouble, matuKrasa, nedelasDiena);
                personList.Add(cilveks);
                cilveks.Drink();

                Programmer programmer = new Programmer(vards, augumsDouble, svarsDouble, matuKrasa, valoda);
                programmersList.Add(programmer);

                Animal animal = new Animal();
                animal.Drink();

                walkingList.Add(cilveks);
                walkingList.Add(animal);

                /* IWalking walking;
                 if(turpinat == "y")
                 {
                     walking = new Animal();
                 }
                 else {
                     walking = new Person(vards, augumsDouble, svarsDouble, matuKrasa, nedelasDiena);
                 }
                 walking.DoWalking();
 */

                Console.WriteLine("Turpināt? y/n ");
                turpinat = Console.ReadLine();
            }

            foreach (Person person in personList)
            {
                person.WritePersonInfo();
            }

            foreach (Programmer programmer in programmersList)
            {
                programmer.WritePersonInfo();
            }

            foreach (IWalking walking in walkingList)
            {
                walking.DoWalking();
            }

            //Person Cilveks = new Person("Jānis", 189, 85, "brūna", DayOfWeek.Friday);
            //Cilveks.WritePersonInfo();

            // interfeiss (add new file - empty interface) - nosaka vienu metodi, kurai jābūt visās klasēs, kurās tā ir implementēta
            // tādējādi var saīsināt kodu, nevis izsaucot katras klases listi ar metodi, bet izsaucot
            // tikai interfeisa metodes listi. metode klasē jādefinē oblgāti, taču tās parametri katrā klasē
            // var atšķirties. attiecīgi - izsauc metodi vienreiz, taču tā var darīt dažādas lietas atkarībā no klases.

            // līdzigi interfeisam ir arī abstraktā metode - tā arī obligāti jāieleik klasēs, kuras to manto, taču klasē tā jā - override!!
            // + virtuāla metode - var izsaukt no visām klasēm, bet atšķirībā no abstraktās - arī pārrkastīt


            //abstraktās klases - ja vairākām klasēm ir vienāda funkcionalitāte vai kādi vienoti aprēķini utt, to ieraksta abstraktā klasē, kuru manto citas klases
            //klases var mantot tikai vienu abstrakto klasi

            // virtuāla metode - var izsaukt no visām klašem, bet arī pārrkastīt
        }
    }
}
