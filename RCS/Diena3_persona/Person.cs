﻿using System;
namespace Diena3_persona
{
    public class Person : Food, IWalking
    {
        public string Name;
        public double Height;
        public double Weight;
        public string HairColor;
        public DayOfWeek FavoriteWeekDay;
        //public double Kilometri;

        public Person (string name, double height, double weight, string hairColor, DayOfWeek favoriteWeekday)
        {
            Name = name;
            Height = height;
            Weight = weight;
            HairColor = hairColor;
            FavoriteWeekDay = favoriteWeekday;
         //   Kilometri = kilometri;

        }

        public virtual void WritePersonInfo()
        {

            Console.WriteLine("Persona " + Name + " ir " + Height +" cm augumā un sver " + Weight + " kg.");
            Console.WriteLine("Matu krāsa: " + HairColor + ", mīļākā nedēļas diena: " + FavoriteWeekDay);
        }

        public void DoWalking()
        {
            Console.WriteLine("Persona ir nogājusi 10 kilometrus.");
        }

        public override void Drink() // klasē pārraksta virtuālo metodi
        {
            Console.WriteLine("I drink tea");
        }

}



}
