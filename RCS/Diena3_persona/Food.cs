﻿using System;
namespace Diena3_persona
{
    public abstract class Food
    {

        // public abstract void Drink();

        public virtual void Drink()
        {
            Console.WriteLine("I drink water");
        }

        public void Eat()
        {
            Console.WriteLine("I like food.");
        } 
    }
}
