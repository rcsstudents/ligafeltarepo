﻿using System;
namespace Diena3_persona
{
    public class Programmer : Person

    {
        public string Valoda;

        public Programmer(string name, double height, double weight, string hairColor, string valoda):
        base(name, height, weight, hairColor, DayOfWeek.Saturday)
        {
            Valoda = valoda;
        }

        public override void WritePersonInfo()
        {

            Console.WriteLine("Programmētājs " + Name + " ir " + Height + " cm augumā un sver " + Weight + " kg.");
            Console.WriteLine("Matu krāsa: " + HairColor);
            Console.WriteLine("Programmēšanas valoda: " + Valoda);
        }
    }
}