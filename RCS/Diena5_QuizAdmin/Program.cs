﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace Diena5_QuizAdmin
{
    class MainClass
    {
        public static void Main(string[] args)
        {

            QuizQuestion quizQuestion = new QuizQuestion();

           // File.WriteAllText("QuizAdmin.txt", String.Empty);

            string turpinat = "";
            while (turpinat != "n")

            {

                Console.WriteLine("Ieraksti jautājumu: ");
                quizQuestion.Question = Console.ReadLine();

                Console.WriteLine("Ieraksti pareizo atbildi: ");
                quizQuestion.Answer = Console.ReadLine();


                string quizData = JsonConvert.SerializeObject(quizQuestion);
                using (StreamWriter writer = File.AppendText("QuizAdmin.txt"))
                {
                    writer.WriteLine(quizData);
                }

                Console.WriteLine("Turpināt? y/n ");
                turpinat = Console.ReadLine();
            }


                string atgriezt = "";

                Console.WriteLine("Vai atgriezt datus? y/n ");
                atgriezt = Console.ReadLine();



            if (atgriezt == "y")
            {

                    string dataFromFile;
                    using (StreamReader reader = new StreamReader("QuizAdmin.txt"))

                    {
                        dataFromFile = reader.ReadToEnd();
                    }

                    QuizQuestion deserialized = JsonConvert.DeserializeObject<QuizQuestion>(quizData);

                    Console.WriteLine(deserialized.Question);
                    Console.WriteLine(deserialized.Answer);

                } 
        }
    }
    }

