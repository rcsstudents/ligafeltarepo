﻿using System;
namespace Diena5_QuizAdmin
{
    [Serializable]
    public class QuizQuestion
    {

        public string Question { get; set; }
        public string Answer { get; set; }

    }
}
