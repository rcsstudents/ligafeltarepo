﻿using System;
using System.Collections.Generic;

namespace Diena1
{
    class MainClass
    {
        static void Main(string[] args)
        {
            List<string> userAnswers = new List<string>();

            int score = 0;
            foreach (QuizQuestion question in DefinedQuestions.quizQuestion)
            {
                Console.WriteLine(question.Question);
                string answer = Console.ReadLine();
                if (answer == question.Answer)
                {
                    score++;
                }
                userAnswers.Add(answer);
            }

            for (int i = 0; i < userAnswers.Count; i++)
            {
                Console.WriteLine("Pareizā atbilde: " + DefinedQuestions.quizQuestion[i].Answer);
                Console.WriteLine("Tu atbildēji: " + userAnswers[i]);
            }
            Console.WriteLine("Punktu skaits: " + score + "\n"
                              + "Maksimālais punktu skaits bija: "+userAnswers.Count);
        }

    
}

    /*
int i = 0;
            while(i < DefinedQuestions.quizQuestion.Count)

            //for (int i = 0; i < DefinedQuestions.quizQuestion.Count; i++)
            {
                Console.WriteLine(DefinedQuestions.quizQuestion[i].Question);
                answer += Console.ReadLine() + "\n";
                i++;
            } vai

foreach (QuizQuestion question in DefinedQuestions.quizQuestion)
            {
                Console.WriteLine(question.Question);
                answer += Console.ReadLine() + "\n";
            }
    */
}
