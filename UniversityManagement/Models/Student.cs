﻿using System;
namespace Models
{
    // class library : ietver klases, bet tajā netiek izpildīta programma

    public class Student
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public int UniversityId { get; set; }
    }
}
