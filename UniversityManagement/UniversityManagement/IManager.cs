﻿using System;
namespace UniversityManagement
{
    public interface IManager
    {
        void Create();
        void Read();
        void Update();
        void Delete();
    }
}
