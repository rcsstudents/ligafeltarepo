﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataManager;
using Models;

namespace UniversityManagement.Manager
{
    public class UniversityManager : IManager
    {
        List<University> Universities;
        DataReader reader;
        DataWriter writer;

        public UniversityManager()
        {
            reader = new DataReader(DataConfiguration.UniversityData);
            writer = new DataWriter(DataConfiguration.UniversityData);
            Universities = reader.ReadData<University>();
        }

        public void Create()
        {
            University university = new University();
            Console.WriteLine("University ID: ");
            string universityID = Console.ReadLine();
            university.Id = Convert.ToInt32(universityID);
            Console.WriteLine("University title: ");
            university.Name = Console.ReadLine();

            Universities.Add(university);
            writer.WriteData(Universities);
        }

        public void Delete()
        {
            Console.WriteLine("University ID: ");
            string universityID = Console.ReadLine();
            int id = Convert.ToInt32(universityID);
            Universities = Universities.Where(u => u.Id != id).ToList();
            writer.WriteData(Universities);
        }

        public void Read()
        {
            foreach(University university in Universities)
            {
                Console.WriteLine(university.Id + " " + university.Name);
            }
        }

        public void Update()
        {
            Console.WriteLine("University ID: ");
            string universityID = Console.ReadLine();
            int id = Convert.ToInt32(universityID);
            University university = Universities.FirstOrDefault(u => u.Id == id);
            if (university==null)
            {
                Console.WriteLine("University with an Id " + universityID + " does not exist.");
                return;
            }

            Console.WriteLine("New title: ");
            university.Name = Console.ReadLine();

            writer.WriteData(Universities);
        }
    }
}
