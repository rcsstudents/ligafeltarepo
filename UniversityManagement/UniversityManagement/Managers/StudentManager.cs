﻿using System;
using System.Collections.Generic;
using Models;
using DataManager;
using System.Linq;
using UniversityManagement;

namespace UniversityManagement.Manager
{
    public class StudentManager : IManager
    {
        List<Student> Students;
        DataReader reader;
        DataWriter writer;

        public StudentManager()
        {
            reader = new DataReader(DataConfiguration.StudentData);
            writer = new DataWriter(DataConfiguration.StudentData);
            Students = reader.ReadData<Student>();

        }

        public void Create()
        {
            Student student = new Student();
            Console.WriteLine("Student ID: ");
            string studentID = Console.ReadLine();
            student.Id = Convert.ToInt32(studentID);
            Console.WriteLine("Students Name: ");
            student.Name = Console.ReadLine();
            Console.WriteLine("Students Surname: ");
            student.Surname = Console.ReadLine();
            Console.WriteLine("University Id: ");
            string universityID = Console.ReadLine();
            student.UniversityId = Convert.ToInt32(universityID);

            Students.Add(student);
            writer.WriteData(Students);
        }

        public void Delete()
        {
            Console.WriteLine("Students ID: ");
            string studentID = Console.ReadLine();
            int id = Convert.ToInt32(studentID);
            Students = Students.Where(u => u.Id != id).ToList();
            writer.WriteData(Students);
        }

        public void Read()
        {
            UniversityManager universityManager = new UniversityManager();
            foreach (Student student in Students)
            {
                Console.WriteLine("Student " + student.Id + " " + student.Name + " " + student.Surname + " studies in: ");
                University studentsUniversity = universityManager.Universities.FirstOrDefault(u => u.Id == student.UniversityId);
                Console.WriteLine(studentsUniversity.Id + " " + studentsUniversity.Name);
            }
        }

        public void Update()
        {
            Console.WriteLine("Students ID: ");
            string studentID = Console.ReadLine();
            int id = Convert.ToInt32(studentID);
            Student student = Students.FirstOrDefault(s => s.Id == id);
            if (student == null)
            {
                Console.WriteLine("Student with an Id " + studentID + " does not exist.");
                return;
            }

            Console.WriteLine("New ID: ");
            string newID = Console.ReadLine();
            student.Id = Convert.ToInt32(newID);
            Console.WriteLine("New Name: ");
            student.Name = Console.ReadLine();
            Console.WriteLine("New Surname: ");
            student.Surname = Console.ReadLine();
            Console.WriteLine("ID of the new University: ");
            string newUniID = Console.ReadLine();
            student.UniversityId = Convert.ToInt32(newUniID);

            writer.WriteData(Students);
        }
    }
}
